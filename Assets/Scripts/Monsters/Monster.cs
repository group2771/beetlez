using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Monster : MonoBehaviour
{
    [HideInInspector] public CharacterController _characterController;
    [SerializeField] private Transform _heart;
    public IMovable _movement;
    public IAnimatable _animation;
    private void Awake()
    {
        _movement = GetComponent<IMovable>();
        _animation = GetComponent<IAnimatable>();
        _characterController = GetComponent<CharacterController>();
    }
    private void Start()
    {
        _movement.StartMove(_characterController, transform);
    }
    private void Update()
    {
        _animation.ChangeAnimationState(_movement.ReturnMoveState());
        _heart.LookAt(RotateCamera.cameraTransform);
    }
}
