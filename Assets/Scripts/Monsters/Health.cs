using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Animator[] _animators;
    private int amountOfLives;
    private const string _oneLive = "1 Live";
    private const string _twoLives = "2 Lives";
    private const string _threeLives = "3 Lives";
    private const string _fourLives = "4 Lives";
    private const string _toTwoLives = "To 2 Lives";
    private const string _toThreeLives = "To 3 Lives";

    void Start()
    {
        amountOfLives = Random.Range(1, DifficultyData.amountOfLives + 1);
        switch (amountOfLives)
        {
            case 1:
                _animator.Play(_oneLive);
                break;
            case 2:
                _animator.Play(_twoLives);
                break;
            case 3:
                _animator.Play(_threeLives);
                break;
            case 4:
                _animator.Play(_fourLives);
                break;
        }
    }
    public void LoseLive()
    {
        amountOfLives--;
        ChangeAnimationState();
        if (amountOfLives == 0)
        {
            GlobalEventManager.SendKilled(transform.gameObject);
        }
    }

    private void ChangeAnimationState()
    {
        switch (amountOfLives)
        {
            case 1:
                _animators[0].Play("Die");
                break;
            case 2:
                _animator.Play(_toTwoLives);
                _animators[1].Play("Die");
                break;
            case 3:
                _animator.Play(_toThreeLives);
                _animators[2].Play("Die");
                break;
        }
    }
}
