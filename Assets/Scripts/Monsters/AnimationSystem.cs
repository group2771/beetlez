using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSystem : MonoBehaviour, IAnimatable
{
    [SerializeField] private Animator _animator;
    private const string _idleAnimation = "Idle";
    private const string _moveAnimation = "Move";

    private void Update()
    {
        _animator.speed = DifficultyData.moveSpeedScale;
    }

    public void ChangeAnimationState(MonsterMovementSystem.MoveState moveState)
    {
        switch (moveState)
        {
            case MonsterMovementSystem.MoveState.Idle:
                _animator.Play(_idleAnimation);
                break;
            case MonsterMovementSystem.MoveState.Move:
                _animator.Play(_moveAnimation);
                break;
        }
    }
}
