using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpMovement : MonsterMovementSystem
{
    private void Start()
    {
        _targetSystem._spawnDistance = 1f;
    }

    public override void StartMove(CharacterController characterController, Transform transform)
    {
        StartCoroutine(StartMoveCoroutine(characterController, transform));
    }

    public override IEnumerator StartMoveCoroutine(CharacterController characterController, Transform monsterTransform)
    {
        yield return CheckGroundCoroutine(characterController);
        StartCoroutine(GravityCoroutine(characterController));
        while (true)
        {
            _targetSystem.ChangePositionOfSphere(monsterTransform.position.y);
            yield return RotateToTarget(monsterTransform);
            moveState = MoveState.Move;
            yield return MoveToTarget(characterController, monsterTransform);
            moveState = MoveState.Idle;
            yield return null;
        }
    }

    public override IEnumerator MoveToTarget(CharacterController characterController, Transform monsterTransform)
    {
        Vector3 direction = Vector3.zero;
        float jumpForce = 11f;
        while (!_checkGround.IsGrounded())
        {
            yield return null;
        }
        direction.y += jumpForce;
        float i = 5f;
        while (true)
        {
            if (direction.y < -gravity && _checkGround.IsGrounded())
            {
                break;
            }
            direction.y -= i * Time.deltaTime;
            characterController.Move((monsterTransform.forward * basicMoveSpeed * 3 * Time.deltaTime) + direction * Time.deltaTime);
            yield return null;
        }
    }
}
