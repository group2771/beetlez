using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonsterMovementSystem : MonoBehaviour, IMovable
{
    public const float gravity = -9.1f;
    [HideInInspector] public MoveState moveState = MoveState.Idle;
    [SerializeField] public float basicMoveSpeed = 4f;
    [SerializeField] public float basicRotationSpeed = 1f;
    private float rotationSpeed;
    [HideInInspector] public float moveSpeed;
    public TargetSystem _targetSystem;
    public CheckGround _checkGround;
    public enum MoveState
    {
        Idle,
        Move
    }
    private void Start()
    {
        moveSpeed = basicMoveSpeed;
        rotationSpeed = basicRotationSpeed;
    }
    private void Update()
    {
        UpdateSpeed();
    }

    private void UpdateSpeed()
    {
        moveSpeed = basicMoveSpeed * DifficultyData.moveSpeedScale;
        rotationSpeed = basicRotationSpeed * DifficultyData.moveSpeedScale;
    }
    public MoveState ReturnMoveState()
    {
        return moveState;
    }
    public abstract void StartMove(CharacterController characterController, Transform transform);
    public abstract IEnumerator StartMoveCoroutine(CharacterController characterController, Transform monsterTransform);
    internal IEnumerator CheckGroundCoroutine(CharacterController characterController)
    {
        while (!_checkGround.IsGrounded())
        {
            //characterController.Move(new Vector3(0, gravity, 0) * Time.deltaTime);
            ApplyGravity(characterController);
            yield return null;
        }
    }
    public void ApplyGravity(CharacterController characterController)
    {
        characterController.Move(new Vector3(0, gravity, 0) * Time.deltaTime);
    }
    internal IEnumerator RotateToTarget(Transform monsterTransform)
    {
        Quaternion lookRotation = Quaternion.LookRotation(_targetSystem.targetPosition - monsterTransform.position);
        Quaternion previousRotation = monsterTransform.rotation;
        float angle = Quaternion.Angle(previousRotation, lookRotation);
        float time = 0;
        while (time < 1)
        {
            time += Time.deltaTime * rotationSpeed * (360 / angle);
            monsterTransform.rotation = Quaternion.Slerp(previousRotation, lookRotation, time);
            yield return null;
        }
    }
    public IEnumerator GravityCoroutine(CharacterController characterController)
    {
        while (true)
        {
            if (!_checkGround.IsGrounded())
            {
                ApplyGravity(characterController);
            }
            yield return null;
        }
    }
    public abstract IEnumerator MoveToTarget(CharacterController characterController, Transform monsterTransform);
}
