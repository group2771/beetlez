using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMonsterMovement : MonsterMovementSystem
{
    public override void StartMove(CharacterController characterController, Transform transform)
    {
        StartCoroutine(StartMoveCoroutine(characterController, transform));
    }

    public override IEnumerator StartMoveCoroutine(CharacterController characterController, Transform monsterTransform)
    {
        yield return CheckGroundCoroutine(characterController);
        StartCoroutine(GravityCoroutine(characterController));
        while (true)
        {
            _targetSystem.ChangePositionOfSphere(monsterTransform.position.y);
            yield return RotateToTarget(monsterTransform);
            moveState = MoveState.Move;
            yield return MoveToTarget(characterController, monsterTransform);
            moveState = MoveState.Idle;
            yield return null;
        }
    }

    public override IEnumerator MoveToTarget(CharacterController characterController, Transform monsterTransform)
    {
        while (Vector3.Distance(monsterTransform.position, _targetSystem.targetPosition) > _targetSystem.minDistance)
        {
            if (!_targetSystem.IsRaycastSphere(monsterTransform))
            {
                yield return StartCoroutine(RotateToTarget(monsterTransform));
            }
            else
            {
                characterController.Move(monsterTransform.forward * moveSpeed * Time.deltaTime);
                yield return null;
            }
        }
    }
}
