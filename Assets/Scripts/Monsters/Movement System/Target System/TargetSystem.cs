using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSystem : MonoBehaviour
{
    [HideInInspector] public Vector3 targetPosition;
    [HideInInspector] public Transform _targetSphereCollider;
    [SerializeField] private GameObject _targetSphereColliderPrefab;
    [SerializeField] private LayerMask _layerMask;
    [HideInInspector] public float minDistance = 0.2f;
    [HideInInspector] public float _spawnDistance = 10f;

    public void ChangeVector3(float y)
    {
        targetPosition = new Vector3(Random.Range(-_spawnDistance, _spawnDistance), y, Random.Range(-_spawnDistance, _spawnDistance));
    }

    public void ChangePositionOfSphere(float y)
    {
        if(_targetSphereCollider == null)
        {
            _targetSphereCollider = Instantiate(_targetSphereColliderPrefab).transform;
        }
        targetPosition = new Vector3(Random.Range(-_spawnDistance, _spawnDistance), y, Random.Range(-_spawnDistance, _spawnDistance));
        _targetSphereCollider.position = targetPosition;
    }

    public bool IsRaycastSphere(Transform transform)
    {
        RaycastHit _hit;
        Ray _ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(_ray, out _hit, 1000f, _layerMask))
        {
            if (_hit.transform == _targetSphereCollider)
            {
                return true;
            }
        }
        return false;
    }

    private void OnDestroy()
    {
        if(_targetSphereCollider != null)
        {
            Destroy(_targetSphereCollider.gameObject);
        }
    }
}
