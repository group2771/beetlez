using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportMonsterMovement : MonsterMovementSystem, IMovable
{
    private float teleportFrequency = 0.1f;
    private float teleportPause = 1f;
    private void Start()
    {
        moveState = MoveState.Idle;
    }
    public override void StartMove(CharacterController characterController, Transform transform)
    {
        StartCoroutine(StartMoveCoroutine(characterController, transform));
    }

    public override IEnumerator StartMoveCoroutine(CharacterController characterController, Transform monsterTransform)
    {
        yield return CheckGroundCoroutine(characterController);
        while (true)
        {
            for (int i = 0; i < 10; i++)
            {
                _targetSystem.ChangePositionOfSphere(monsterTransform.position.y);
                yield return MoveToTarget(characterController, monsterTransform);
            }
            yield return new WaitForSeconds(teleportPause);
        }
    }
    public override IEnumerator MoveToTarget(CharacterController characterController, Transform monsterTransform)
    {
        yield return new WaitForSeconds(teleportFrequency);
        monsterTransform.position = _targetSystem.targetPosition;
    }
}
