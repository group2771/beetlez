using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    [SerializeField] private float _distance = 1.2f;
    [SerializeField] private LayerMask _layerMask;

    public bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, _distance);
    }
}
