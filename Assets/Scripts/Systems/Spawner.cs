using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TargetSystem))]
public class Spawner : MonoBehaviour
{
    private TargetSystem targetSystem;
    [SerializeField] GameObject[] _monsterPrefabs;
    private List<GameObject> _monsters = new List<GameObject>();
    [SerializeField] private float minSpawnDelay = 3f;
    [SerializeField] private float maxSpawnDelay = 5f;
    [SerializeField] private float _y = 5;
    [SerializeField] private float amountOfMonstersToLose = 10;
    private bool isToFreeze = false;

    private void Awake()
    {
        targetSystem = transform.gameObject.GetComponent<TargetSystem>();
        GlobalEventManager.OnKillAll.AddListener(KillAll);
        GlobalEventManager.OnKilled.AddListener(DestroyKilled);
        GlobalEventManager.OnFreeze.AddListener(SetIsToFreezeTrue);
    }

    private void Start()
    {   
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            if (isToFreeze)
            {
                yield return new WaitForSeconds(3f);
                isToFreeze = false;
            }
            targetSystem.ChangeVector3(_y);
            _monsters.Add(Instantiate(GetRandomType(), targetSystem.targetPosition, Quaternion.identity));
            if (_monsters.Count >= amountOfMonstersToLose)
            {
                GlobalEventManager.SendGameLoosed();
            }
            yield return new WaitForSeconds(SpawnDelay(DifficultyData.spawnSpeedScale));
        }
    }

    private void SetIsToFreezeTrue()
    {
        isToFreeze = true;
    }

    private void DestroyKilled(GameObject monsterGameObject)
    {
        _monsters.Remove(monsterGameObject);
        Destroy(monsterGameObject);
    }

    private void KillAll()
    {
        var amountOfDestroyed = 0;
        foreach (GameObject monster in _monsters)
        {
            Destroy(monster);
            amountOfDestroyed++;
        }
        _monsters = new List<GameObject>();
        GlobalEventManager.SendAllDestroed(amountOfDestroyed);
    }

    private float SpawnDelay(float difficultyScale)
    {
        return Mathf.Pow(Random.Range(minSpawnDelay, maxSpawnDelay), 1 / difficultyScale);
    }
    private GameObject GetRandomType()
    {
        int i = Random.Range(0, 101);
        if(i >= 85)
        {
            return _monsterPrefabs[2];
        }
        else if (i >= 75)
        {
            return _monsterPrefabs[1];
        }
        else
        {
            return _monsterPrefabs[0];
        }
    }
}
