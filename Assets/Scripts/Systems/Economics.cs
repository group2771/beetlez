using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Economics : MonoBehaviour
{
    private int counterOfKilled = 0;
    private int coinsCounter = 0;
    private int i = 0;
    private void Awake()
    {
        GlobalEventManager.OnKilled.AddListener(EnemyKilled);
        GlobalEventManager.OnAllDestroyed.AddListener(EnemyKilled);
        GlobalEventManager.OnShop.AddListener(Pay);
        GlobalEventManager.OnUpdateAviliables.AddListener(UpdateAvialiables);
        GlobalEventManager.OnGameLoosed.AddListener(EndGame);
    }
    private void Start()
    {
        counterOfKilled = 0;
    }

    private void EnemyKilled(GameObject gameObject)
    {
        counterOfKilled++;
        i++;
        if (i == 3)
        {
            i = 0;
            coinsCounter++;
            GlobalEventManager.SendAmountOfCoinsChanged(coinsCounter);
        }
        GlobalEventManager.SendAmountOfKilledChanged(counterOfKilled);
    }

    private void EnemyKilled(int amountOfKilled)
    {
        counterOfKilled += amountOfKilled;
        GlobalEventManager.SendAmountOfKilledChanged(counterOfKilled);
    }

    private void Pay(int cost)
    {
        coinsCounter -= cost;
        GlobalEventManager.SendAmountOfCoinsChanged(coinsCounter);
    }

    private void UpdateAvialiables()
    {
        GlobalEventManager.SendAmountOfCoinsChanged(coinsCounter);
    }

    private void EndGame()
    {
        if (counterOfKilled > PlayerPrefs.GetInt("Record"))
        {
            PlayerPrefs.SetInt("Record", counterOfKilled);
        }
        ScenesLoader.Load("Menu");
    }
}
