using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DifficultyData : object
{
    public static float spawnSpeedScale;
    public static float moveSpeedScale;
    public static int amountOfLives;
}
