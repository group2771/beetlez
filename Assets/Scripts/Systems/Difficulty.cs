using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Difficulty : MonoBehaviour
{
    private bool isToFreeze = false;

    private void Awake()
    {
        DifficultyData.amountOfLives = 1;
        DifficultyData.moveSpeedScale = 1;
        DifficultyData.spawnSpeedScale = 1;
        GlobalEventManager.OnFreeze.AddListener(SetIsToFreezeTrue);
    }

    private void Start()
    {
        StartCoroutine(ScaleCoroutine());
    }

    private void Update()
    {
        if (DifficultyData.spawnSpeedScale > 2.5)
        {
            DifficultyData.amountOfLives = 4;
        }
        else if (DifficultyData.spawnSpeedScale > 1.7)
        {
            DifficultyData.amountOfLives = 3;
        }
        else if (DifficultyData.spawnSpeedScale > 1.3)
        {
            DifficultyData.amountOfLives = 2;
        }
    }

    private IEnumerator ScaleCoroutine()
    {
        while (true)
        {
            if (isToFreeze)
            {
                yield return new WaitForSeconds(3f);
                GlobalEventManager.SendFreezeEnded();
                GlobalEventManager.SendUpdateAviliables();
                isToFreeze = false;
            }
            DifficultyData.spawnSpeedScale = DifficultyData.spawnSpeedScale + (DifficultyData.spawnSpeedScale * 0.02f * Time.deltaTime);
            DifficultyData.moveSpeedScale = DifficultyData.moveSpeedScale + (DifficultyData.moveSpeedScale * 0.006f * Time.deltaTime);
            yield return null;
        }
    }

    private void SetIsToFreezeTrue()
    {
        isToFreeze = true;
    }
}
