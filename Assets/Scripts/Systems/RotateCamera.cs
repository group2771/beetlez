using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    [SerializeField] private float speed;
    public static Transform cameraTransform;
    private void Awake()
    {
        cameraTransform = transform.GetChild(0).transform;
    }
    void Update()
    {
        transform.Rotate(new Vector3(0, speed, 0) * Time.deltaTime);
    }
}
