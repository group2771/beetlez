using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Taps : MonoBehaviour
{
    private Camera _mainCamera;
    private Ray _ray;
    private RaycastHit _hit;
    [SerializeField] private LayerMask _layerMask;

    private void Start()
    {
        _mainCamera = Camera.main;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_ray, out _hit, 1000f, _layerMask))
            {
                if (_hit.transform != null)
                {
                    GlobalEventManager.SendLoseLive(_hit.transform.parent.gameObject);
                    Health health = _hit.transform.parent.gameObject.transform.GetComponent<Health>();
                    health.LoseLive();
                    //GlobalEventManager.SendKilled(_hit.transform.parent.gameObject);
                }
            }
        }
    }
}
