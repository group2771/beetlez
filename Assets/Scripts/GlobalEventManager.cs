using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GlobalEventManager : MonoBehaviour
{
    public static UnityEvent<GameObject> OnLoseLive = new UnityEvent<GameObject>();

    public static void SendLoseLive(GameObject monsterGameObject)
    {
        OnLoseLive.Invoke(monsterGameObject);
    }

    public static UnityEvent<GameObject> OnKilled = new UnityEvent<GameObject>();

    public static void SendKilled(GameObject monsterGameObject)
    {
        OnKilled.Invoke(monsterGameObject);
    }

    public static UnityEvent OnKillAll = new UnityEvent();

    public static void SendKillAll()
    {
        OnKillAll.Invoke();
    }

    public static UnityEvent<int> OnAmountOfKilledChanged = new UnityEvent<int>();

    public static UnityEvent<int> OnAllDestroyed = new UnityEvent<int>();

    public static void SendAllDestroed(int amountOfDestroyed)
    {
        OnAllDestroyed.Invoke(amountOfDestroyed);
    }

    public static void SendAmountOfKilledChanged(int counterOfKilled)
    {
        OnAmountOfKilledChanged.Invoke(counterOfKilled);
    }

    public static UnityEvent<int> OnAmountOfCoinsChanged = new UnityEvent<int>();

    public static void SendAmountOfCoinsChanged(int coinsCounter)
    {
        OnAmountOfCoinsChanged.Invoke(coinsCounter);
    }

    public static UnityEvent OnUpdateAviliables = new UnityEvent();

    public static void SendUpdateAviliables()
    {
        OnUpdateAviliables.Invoke();
    }

    public static UnityEvent<int> OnShop = new UnityEvent<int>();

    public static void SendShop(int cost)
    {
        OnShop.Invoke(cost);
    }

    public static UnityEvent OnFreeze = new UnityEvent();

    public static void SendFreeze()
    {
        OnFreeze.Invoke();
    }

    public static UnityEvent OnFreezeEnded = new UnityEvent();

    public static void SendFreezeEnded()
    {
        OnFreezeEnded.Invoke();
    }

    public static UnityEvent OnGameLoosed = new UnityEvent();

    public static void SendGameLoosed()
    {
        OnGameLoosed.Invoke();
    }
}
