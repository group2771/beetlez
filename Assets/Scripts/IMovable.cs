using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable
{
    void StartMove(CharacterController characterController, Transform monsterTransform);
    MonsterMovementSystem.MoveState ReturnMoveState();
}
public interface IAnimatable
{
    void ChangeAnimationState(MonsterMovementSystem.MoveState moveState);
}