using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesLoader
{
    public static void Load(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
