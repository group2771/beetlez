using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameButton : Button
{
    public override void OnButtonPressed()
    {
        ScenesLoader.Load("Game");
    }
}
