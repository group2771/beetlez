using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGameButton : Button
{
    public override void OnButtonPressed()
    {
        Application.Quit();
    }
}
