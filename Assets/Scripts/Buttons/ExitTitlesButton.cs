using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTitlesButton : Button
{
    public override void OnButtonPressed()
    {
        ScenesLoader.Load("Menu");
    }
}
