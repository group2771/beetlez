using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTitlesButton : Button
{
    public override void OnButtonPressed()
    {
        ScenesLoader.Load("Titles");
    }
}
