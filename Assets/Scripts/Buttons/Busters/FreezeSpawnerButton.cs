using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeSpawnerButton : Buster
{
    public bool isFreezing = false;

    private void Start()
    {
        GlobalEventManager.OnFreezeEnded.AddListener(SetIsFreezingToFalse);
        cost = 2;
    }

    public override void OnButtonPressed()
    {
        button.SetActive(false);
        isFreezing = true;
        GlobalEventManager.SendShop(cost);
        GlobalEventManager.SendFreeze();
    }
    public override void isAvialible(int coinsCounter)
    {
        if (!isFreezing && coinsCounter >= cost)
        {
            button.SetActive(true);
        }
        else
        {
            button.SetActive(false);
        }
    }
    private void SetIsFreezingToFalse()
    {
        isFreezing = false;
    }
}
