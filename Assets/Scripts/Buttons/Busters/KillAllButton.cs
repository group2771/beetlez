using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAllButton : Buster
{
    private void Start()
    {
        cost = 5;
    }
    public override void OnButtonPressed()
    {
        GlobalEventManager.SendKillAll();
        GlobalEventManager.SendShop(cost);
    }
    public override void isAvialible(int coinsCounter)
    {
        if(coinsCounter >= cost)
        {
            button.SetActive(true);
        }
        else
        {
            button.SetActive(false);
        }
    }
}
