using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Buster : MonoBehaviour
{
    public GameObject button;
    public int cost;
    public abstract void OnButtonPressed();
    public abstract void isAvialible(int coinsCounter);
    private void Awake()
    {
        GlobalEventManager.OnAmountOfCoinsChanged.AddListener(isAvialible);
    }
}
