using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeKilledEnemiesText : MonoBehaviour
{
    TextMeshProUGUI _text;
    private void Awake()
    {
        GlobalEventManager.OnAmountOfKilledChanged.AddListener(ChangeText);
        _text = GetComponent<TextMeshProUGUI>();
    }
    private void ChangeText(int counterOfKilled)
    {
        _text.text = counterOfKilled.ToString();
    }
}
