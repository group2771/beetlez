using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeAmountOfCoinsText : MonoBehaviour
{
    TextMeshProUGUI _text;
    private void Awake()
    {
        GlobalEventManager.OnAmountOfCoinsChanged.AddListener(AmountOfCoinsChangedChangeText);
        _text = GetComponent<TextMeshProUGUI>();
    }
    private void AmountOfCoinsChangedChangeText(int coinsCounter)
    {
        _text.text = coinsCounter.ToString();
    }
}
